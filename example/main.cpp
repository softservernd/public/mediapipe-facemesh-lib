
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <vector>
#include <functional>

#include "face_landmarks_detector.h"

int main(int argc, char* argv[]) {
  using namespace Mediapipe;

  PlatformConfiguration m_platformConfig;
  FaceLandmarksDetector detector(&m_platformConfig);

  cv::VideoCapture capture;

  bool fileMode = argc > 1;
  if (fileMode) {
    capture.open(argv[1]);
  } else {
    capture.open(0);
  }

  for (;;) {
    // Capture opencv camera or video frame.
    cv::Mat frame;
    capture >> frame;

    if (frame.empty()) {
      std::cout << "NO FRAME\n";
      continue;
    }

    cv::Mat inputFrame;
    cv::cvtColor(frame, inputFrame, cv::COLOR_BGR2RGB);
    cv::flip(inputFrame, inputFrame, 1);
    cv::flip(frame, frame, 1);

    const auto& landmarks =
        detector.processFrame(inputFrame, double(cv::getTickCount()) /
                                              double(cv::getTickFrequency()) * 1e6);

    for (const auto& landmark : landmarks) {
      cv::drawMarker(frame, landmark, cv::Scalar::all(255),
                     cv::MARKER_TRIANGLE_UP, 1);
    }

    cv::imshow("Mediapipe Test", frame);

    // Press any key to exit.
    const int pressedKey = cv::waitKey(5);
    if (pressedKey >= 0 && pressedKey != 255) break;
  }
}
