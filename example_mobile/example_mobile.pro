QT += qml quick multimedia multimedia-private svg xml

CONFIG += c++17

SOURCES += $$files( "*.cpp" )
HEADERS += $$files( "*.h" )
RESOURCES += $$files( "*.qrc" )
OTHER_FILES += $$files( "*.md" )
OTHER_FILES += $$files( "*.png" )

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

DEFINES += QT_DEPRECATED_WARNINGS

# Modify if building from a non-default location
FACE_LANDMARKS_DETECTOR_DIR = $${PWD}/..

# This needs to be set manually
OPENCV_DIR =

INCLUDEPATH += $${OPENCV_DIR}/include
INCLUDEPATH += $${FACE_LANDMARKS_DETECTOR_DIR}/include

ios {
    # The path to iOS framework location
    OPENCV_FRAMEWORK_DIR = 

    LIBS +=  -F $${OPENCV_FRAMEWORK_DIR} \
             -framework opencv2

    LIBS += $${FACE_LANDMARKS_DETECTOR_DIR}/lib/face_landmarks_detector_ios.dylib
    LIBS += -framework Metal

    FACE_DETECTION_TFLITE.files = $${FACE_LANDMARKS_DETECTOR_DIR}/lib/face_detection_short_range.tflite
    FACE_DETECTION_TFLITE.path = mediapipe/modules/face_detection/

    FACE_LANDMARK_TFLITE.files = $${FACE_LANDMARKS_DETECTOR_DIR}/lib/face_landmark.tflite
    FACE_LANDMARK_TFLITE.path = mediapipe/modules/face_landmark/

    QMAKE_BUNDLE_DATA += FACE_DETECTION_TFLITE FACE_LANDMARK_TFLITE
    QMAKE_INFO_PLIST = Info.plist
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
