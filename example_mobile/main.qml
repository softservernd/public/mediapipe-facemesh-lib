import QtQuick 2.5
import QtQuick.Controls 2.1
import QtMultimedia 5.5
import FilterLib 1.0
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0

ApplicationWindow {
    id: app

    visible: true
    width: 640
    height: 480
    title: qsTr("Video Filter")

    property string videoFilterName: "faceLandmarksFilter"
    property var cameraMeta: findFrontFacingCamera()

    Rectangle {
        anchors.fill: parent
        color: "black"
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        autoOrientation: true
        source: camera
        filters: [ faceLandmarksFilter ]

        Rectangle {
            anchors.fill: cameraDisplayNameText
            anchors.margins: -cameraDisplayNameText.height / 2
            radius: height / 2
            color: "#808080"
            opacity: 0.5
        }

        Rectangle {
            anchors.fill: videoFilterText
            anchors.margins: -videoFilterText.height / 2
            radius: height / 2
            color: "#808080"
            opacity: 0.5
            visible: !videoFilterMenu.visible
        }

        Text {
            id: videoFilterText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.margins: 10
            color: "yellow"
            font.pointSize: 12
            text: videoFilterName
            visible: !videoFilterMenu.visible

            MouseArea {
                anchors.fill: parent
                onClicked: videoFilterMenu.open()
            }
        }
    }

    Menu {
        id: videoFilterMenu

        anchors.centerIn: parent
        width: parent.width / 2

        Repeater {
            model: [ "faceLandmarksFilter" ]
            MenuItem {
                text: modelData
                font.pointSize: 12
                highlighted:  modelData === videoFilterName

                onTriggered: {
                    videoFilterName = modelData;
                    switch ( videoFilterName )
                    {
                        case "faceLandmarskFilter":
                            videoOutput.filters = [ faceLandmarksFilter ];
                            break;
                    }
                }
            }
        }
    }

    Camera {
        id: camera
    }

    FaceLandmarksFilter {
        id: faceLandmarksFilter
    }

    function findFrontFacingCamera() {
        const cams = QtMultimedia.availableCameras

        for ( let idx = 0; idx < cams.length; idx++ ) {
            if ( cams[idx].position === Camera.FrontFace ) {
                return cams[idx];
            }
        }
    }

    Component.onCompleted: {
        camera.stop()
        camera.deviceId = cameraMeta.deviceId
        camera.start()
    }
}
