#include "face_landmarks_detector.h"

#include "mediapipe/framework/calculator_framework.h"
#include "mediapipe/framework/port/logging.h"
#include "mediapipe/framework/port/parse_text_proto.h"
#include "mediapipe/framework/port/status.h"
#include "mediapipe/framework/port/file_helpers.h"
#include "mediapipe/framework/formats/image_frame.h"
#include "mediapipe/framework/formats/image_frame_opencv.h"
#include "mediapipe/framework/formats/landmark.pb.h"
#include <iostream>

#ifdef __ANDROID__
#include "mediapipe/util/android/asset_manager_util.h"
#endif

namespace Mediapipe
{

constexpr char GraphConfig[] = R"pb(
  input_stream: 'input_video'

  input_side_packet : 'num_faces'
  output_stream : 'output_video'
  output_stream : 'multi_face_landmarks'
  output_stream : 'face_count'

  node{
    calculator: 'FlowLimiterCalculator'
    input_stream : 'input_video'
    input_stream : 'FINISHED:output_video'
    input_stream_info : {
      tag_index: 'FINISHED'
      back_edge : true
    }
    output_stream: 'throttled_input_video'
  }

  # Subgraph that detects faces and corresponding landmarks.
  node{
    calculator: 'FaceLandmarksDetectorFrontCpu'
    input_stream : 'IMAGE:throttled_input_video'
    output_stream : 'LANDMARKS:multi_face_landmarks'
    output_stream : 'ROIS_FROM_LANDMARKS:face_rects_from_landmarks'
    output_stream : 'DETECTIONS:face_detections'
    output_stream : 'ROIS_FROM_DETECTIONS:face_rects_from_detections'
    output_stream : 'FACE_COUNT_FROM_LANDMARKS:face_count'
  }

  # Subgraph that renders face - landmark annotation onto the input image.
  node{
    calculator: 'FaceRendererCpu'
    input_stream : 'IMAGE:throttled_input_video'
    input_stream : 'LANDMARKS:multi_face_landmarks'
    input_stream : 'NORM_RECTS:face_rects_from_landmarks'
    input_stream : 'DETECTIONS:face_detections'
    output_stream : 'IMAGE:output_video'
  }
)pb";

constexpr char kInputStream[] = "input_video";
constexpr char kOutputImageStream[] = "output_video";
constexpr char kLandmarksOutputStream[] = "multi_face_landmarks";
constexpr char kFaceCountOutputStream[] = "face_count";

FaceLandmarksDetector::FaceLandmarksDetector(PlatformConfiguration *m_platformConfig)
  : m_graph(std::make_unique<mediapipe::CalculatorGraph>())
{
  mediapipe::CalculatorGraphConfig config = 
      mediapipe::ParseTextProtoOrDie<mediapipe::CalculatorGraphConfig>(GraphConfig);

  absl::Status status = m_graph->Initialize(config);
  if (!status.ok())
    throw std::runtime_error(status.ToString());

  mediapipe::StatusOrPoller imagePollerResult =
      m_graph->AddOutputStreamPoller(kOutputImageStream);
  mediapipe::StatusOrPoller faceCountPollerResult =
      m_graph->AddOutputStreamPoller(kFaceCountOutputStream);
  mediapipe::StatusOrPoller landmarksPollerResult =
      m_graph->AddOutputStreamPoller(kLandmarksOutputStream);

  if (!imagePollerResult.ok())
    throw std::runtime_error(imagePollerResult.status().ToString());
  if (!faceCountPollerResult.ok())
    throw std::runtime_error(faceCountPollerResult.status().ToString());
  if (!landmarksPollerResult.ok())
    throw std::runtime_error(landmarksPollerResult.status().ToString());

  m_imagePoller = std::make_unique<mediapipe::OutputStreamPoller>(
      std::move(imagePollerResult.value()));
  m_faceCountPoller = std::make_unique<mediapipe::OutputStreamPoller>(
      std::move(faceCountPollerResult.value()));
  m_landmarksPoller = std::make_unique<mediapipe::OutputStreamPoller>(
      std::move(landmarksPollerResult.value()));

#ifdef __ANDROID__
  mediapipe::AssetManager* asset_manager = Singleton<mediapipe::AssetManager>::get();
  if (m_platformConfig && m_platformConfig->initAssetManager)
  	m_platformConfig->initAssetManager(asset_manager);
#endif
  
  status = m_graph->StartRun({});
  if (!status.ok()) {
    throw std::runtime_error(status.ToString());
  }
}

FaceLandmarksDetector::~FaceLandmarksDetector()
{
  m_graph->CloseInputStream(kInputStream);
  m_graph->WaitUntilDone();
}

std::vector<cv::Point> FaceLandmarksDetector::processFrame(const cv::Mat &frame, size_t timestamp) const
{
  std::lock_guard<std::mutex> lk(m_pollingMutex);
  
  // Wrap cv::Mat into a mediapipe::ImageFrame
  auto inputFrame = absl::make_unique<mediapipe::ImageFrame>(
      mediapipe::ImageFormat::SRGB, frame.cols, frame.rows,
      mediapipe::ImageFrame::kDefaultAlignmentBoundary);
  cv::Mat inputFrameMat = mediapipe::formats::MatView(inputFrame.get());
  frame.copyTo(inputFrameMat);

  // Send image packet into the graph
  absl::Status additionResult = m_graph
      ->AddPacketToInputStream(kInputStream, mediapipe::Adopt(inputFrame.release())
      .At(mediapipe::Timestamp(timestamp)));

  // Check input addition result
  if (!additionResult.ok()) return {};

  mediapipe::Packet imagePacket;
  if (!m_imagePoller->Next(&imagePacket))
    return {};
  auto &outputFrame = imagePacket.Get<mediapipe::ImageFrame>();

  mediapipe::Packet faceCountPacket;
  if (!m_faceCountPoller->Next(&faceCountPacket))
    return {};
  
  int faceCount = faceCountPacket.Get<int>();
  if (faceCount < 1)
    return {};

  mediapipe::Packet landmarksPacket;
  if (!m_landmarksPoller->Next(&landmarksPacket))
    return {};
  const auto &multiFaceLandmarks =
      landmarksPacket.Get<std::vector<mediapipe::NormalizedLandmarkList>>();
  auto frameSize = frame.size();

  std::vector<cv::Point> landmarks;
  for (const auto& faceLandmarks : multiFaceLandmarks) {
    for (int i = 0; i < faceLandmarks.landmark_size(); ++i) {
     // Denormalizing landmarks
     landmarks.emplace_back(
         int(faceLandmarks.landmark(i).x() * frameSize.width),
         int(faceLandmarks.landmark(i).y() * frameSize.height));
    }
  }

  return landmarks;
}

}
