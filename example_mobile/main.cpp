#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "FaceLandmarksFilter.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType< FaceLandmarksFilter >("FilterLib", 1, 0, "FaceLandmarksFilter" );
    engine.load( QUrl( QLatin1String( "qrc:/main.qml" ) ) );

    return app.exec();
}
