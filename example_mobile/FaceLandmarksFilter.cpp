#include "FaceLandmarksFilter.h"
#include "opencv2/opencv.hpp"
#include "face_landmarks_detector.h"
#include <memory>
#include <filesystem>
#include <iomanip>
#include <QPixmap>
#include <QPainter>
#include <QPen>
#include <private/qvideoframe_p.h>
#include <qopengl.h>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <map>
#include <type_traits>

FaceLandmarksFilter::FaceLandmarksFilter(QObject* parent)
    : QAbstractVideoFilter(parent)
{
}

QVideoFilterRunnable* FaceLandmarksFilter::createFilterRunnable()
{
    return new FaceLandmarksFilterRunnable();
}

FaceLandmarksFilterRunnable::FaceLandmarksFilterRunnable()
    : m_faceLandmarksDetector(std::make_unique<Mediapipe::FaceLandmarksDetector>())
    , m_glCtxFuncs(QOpenGLContext::currentContext()->functions())
{
}

QImage FaceLandmarksFilterRunnable::videoFrameToImage(const QVideoFrame &videoFrame)
{
    QImage image(videoFrame.width(), videoFrame.height(), QImage::Format_ARGB32);
    GLuint textureId = videoFrame.handle().toInt();
    GLuint frameBuffers;
    m_glCtxFuncs->glGenFramebuffers(1, &frameBuffers);
    GLint previousFrameBuffers;
    m_glCtxFuncs->glGetIntegerv(GL_FRAMEBUFFER_BINDING, &previousFrameBuffers);
    m_glCtxFuncs->glBindFramebuffer(GL_FRAMEBUFFER, frameBuffers);
    m_glCtxFuncs->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
    m_glCtxFuncs->glReadPixels(0, 0,  videoFrame.width(), videoFrame.height(), GL_RGBA, GL_UNSIGNED_BYTE, image.bits());
    m_glCtxFuncs->glBindFramebuffer(GL_FRAMEBUFFER, static_cast<GLuint>(previousFrameBuffers));

    return image;
}

cv::Mat FaceLandmarksFilterRunnable::imageToCvMat(const QImage &input)
{
     cv::Mat mat(input.height(), input.width(), CV_8UC4, const_cast<uchar*>(input.bits()),
                 static_cast<size_t>(input.bytesPerLine()));
     return mat.clone();
}

inline QMatrix defineRotation() {
    QMatrix rotation;
    rotation.rotate(180);
    return rotation;
}

QVideoFrame FaceLandmarksFilterRunnable::run(
    QVideoFrame *input,
    const QVideoSurfaceFormat &surfaceFormat,
    RunFlags flags )
{
    Q_UNUSED( flags )
    Q_UNUSED( surfaceFormat )
    
    static QMatrix rotation = defineRotation();
    auto img = videoFrameToImage(*input);
    auto mat = imageToCvMat(img);
    cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);
    size_t timestamp = double(cv::getTickCount()) / double(cv::getTickFrequency() * 1e6);
    auto landmarks = m_faceLandmarksDetector->processFrame(mat, timestamp);

    QPainter painter(&img);
    static QPen pen;
    pen.setWidth(7);
    pen.setColor(Qt::white);
    painter.setPen(pen);
    
    for (const auto &landmark : landmarks) {
        painter.drawPoint(landmark.x, landmark.y);
    }
    
    painter.end();
    return img.transformed(rotation);
}
