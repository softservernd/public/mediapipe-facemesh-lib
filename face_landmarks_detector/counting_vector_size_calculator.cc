
#include "mediapipe/face_landmarks_detector/counting_vector_size_calculator.h"

#include "mediapipe/framework/formats/landmark.pb.h"

namespace mediapipe {

typedef CountingVectorSizeCalculator<std::vector<::mediapipe::NormalizedLandmarkList>> 
    CountingNormalizedLandmarkListVectorSizeCalculator;

REGISTER_CALCULATOR(CountingNormalizedLandmarkListVectorSizeCalculator);
}  // namespace mediapipe
