#pragma once

#ifdef _MSC_VER
    #ifdef COMPILING_FACE_LANDMARKS_DETECTOR_API
        #define FACE_LANDMARKS_DETECTOR_API __declspec(dllexport)
    #else
        #define FACE_LANDMARKS_DETECTOR_API __declspec(dllimport)
    #endif
#else
    #ifdef COMPILING_FACE_LANDMARKS_DETECTOR_API
        #define FACE_LANDMARKS_DETECTOR_API __attribute__((visibility("default")))
    #else
        #define FACE_LANDMARKS_DETECTOR_API
    #endif
#endif
