CMake will download and patch the official MediaPipe library on generation.

### Linux  Instructions

In-order to use the library on Linux you'll need to install:

	1. Bazelisk
	
	2. Python
	
	3. OpenCV

mkdir build

cd build

cmake -G "Unix Makefiles" ../

make



Notes in case of linker issues related to multiple OpenCV installations:

In case you have multiple installations of OpenCV on your Linux or WSL system you would need to specify the path to your OpenCV library directory,
the following files should be updated with current OpenCV library paths:

_deps/mediapipe-src/WORKSPACE

_deps/mediapipe-src/third_party/opencv_linux.BUILD

In order to simplify this adjustment two cmake variables are introduced that will be recognized by cmake, then proper patch will be automatically generated and applied. These variables are OPENCV_DIR and OPENCV_LIB_DIR . Default value for OPENCV_DIR is derived from results of find_package(OpenCV) command and default value for OPENCV_LIB_DIR is ${OPENCV_DIR}/lib . Both variables can be overriden by respective environmnet variables or -D switches of cmake.

### Windows  Instructions

In-order to use the library on Windows you'll need to install:

	1. MSYS and add the location to the PATH variable
	
	2. Bazel (you won't to call it directly, but it is used within the CMake script)
	
	3. Python
	
	4. OpenCV

mkdir build

cd build

cmake -G "Visual Studio 16 2019" ../

This will generate a VS project

### macOS  Instructions

In-order to use the library on macOS you'll need to install:

	1. Bazelisk
	
	2. OpenCV
	
	3. Python

mkdir build

cd build

cmake -G "Xcode" ../

This will generate an Xcode project

### iOS    Instructions

In-order to build an iOS project you'll need to install:

	1. Bazelisk
	
	2. OpenCV
	
	3. Python

	4. Qt5

	5. Download the iOS OpenCV framework (no need to install, just store the framework location)

```
mkdir build
cd build

# This will generate a Makefile:
cmake -G "Unix Makefiles" ../

# This will populate the `lib/` directory with: `face_landmarks_detector_ios.dylib`
make face_landmarks_detector_ios

# This will populate the `lib/` directory with: `face_landmarks_detector.binarypb`, `face_detection_short_range.tflite` and `face_landmark.tflite` files, the will be used by the `example_mobile` Qt project.
make face_landmarks_detector-mobile-resources
```

Next we will prepare the mobile Qt project:

```
cd ../
cd example_mobile

# Edit the `example_mobile.pro`, we need to set the following variables:
#	`OPENCV_DIR` -- the directory where OpenCV includes are stored
#	`OPENCV_FRAMEWORK_DIR` -- the directory where the downloaded framework is, it should contain `opencv2.framework` in it.

mkdir build
cd build

# This will generate an Xcode project
qmake -spec macx-xcode ../example_mobile.pro
```

Next we need to open the Xcode project, go to `General` setting, scroll to `Frameworks, Libraries and Embedded Content` section, find `face_landmarks_detector_ios.dylib` and set it to `Embed & Sign` from the drop down menu.

Connect the iPhone and run the app.

### Launch Instructions

CMake script will copy all the necessary resources to the  directory so that you can run the `face_landmarks_detector-test` binary out of the box.

For a custom set-up you'll have to specify:

	1. The graph file: *.pbtxt

	2. Modules that are used by the graph: *.tfile, these should be placed in a relative path to the WORKING_DIRECTORY, e.g:
		`mediapipe/modules/face_landmark/face_landmark.tfile`
		`mediapipe/modules/face_detection/face_detection.tfile`
