
#pragma once

#include <memory>
#include <string>
#include <vector>
#include <mutex>
#include <functional>

#include "export.h"

namespace cv
{
  class Mat;
  template <typename> class Point_;
  using Point2i = Point_<int>;
  using Point = Point2i;
}

namespace mediapipe
{
  class CalculatorGraph;
  class OutputStreamPoller;
};

namespace Mediapipe
{
class FACE_LANDMARKS_DETECTOR_API PlatformConfiguration
{
#ifdef __ANDROID__
public:
  PlatformConfiguration(std::function<void(void* AssetManager)> assetManager) 
  {
    initAssetManager = assetManager;
  };
  std::function<void(void* AssetManager)> initAssetManager;
#endif
};

class FACE_LANDMARKS_DETECTOR_API FaceLandmarksDetector
{
public:
    FaceLandmarksDetector(PlatformConfiguration *m_platformConfig);
    ~FaceLandmarksDetector();

    std::vector<cv::Point> processFrame(const cv::Mat& frame, size_t timestamp) const;

private:
  std::unique_ptr<mediapipe::CalculatorGraph> m_graph;
  std::unique_ptr<mediapipe::OutputStreamPoller> m_imagePoller;
  std::unique_ptr<mediapipe::OutputStreamPoller> m_faceCountPoller;
  std::unique_ptr<mediapipe::OutputStreamPoller> m_landmarksPoller;
  mutable std::mutex m_pollingMutex;
};

}
