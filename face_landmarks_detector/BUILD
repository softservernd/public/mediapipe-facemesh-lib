package(default_visibility = [
    "//visibility:public",
])

load(
    "@build_bazel_rules_apple//apple:ios.bzl",
    "ios_application",
    "ios_framework"
)
load(
    "//mediapipe/examples/ios:bundle_id.bzl",
    "BUNDLE_ID_PREFIX",
    "example_provisioning",
)
load(
    "//mediapipe/framework/tool:mediapipe_graph.bzl",
    "mediapipe_simple_subgraph",
)

# Normalized face landmarks vector count calculator
cc_library(
    name = "counting_vector_size_calculator",
    srcs = ["counting_vector_size_calculator.cc"],
    hdrs = ["counting_vector_size_calculator.h"],
    visibility = [
        "//visibility:public",
    ],
    deps = [
        "//mediapipe/framework:calculator_framework",
        "//mediapipe/framework/formats:landmark_cc_proto",
    ],
    alwayslink = 1,
)

# Graphs
mediapipe_simple_subgraph(
    name = "face_landmarks_detector_cpu",
    graph = "face_landmarks_detector_cpu.pbtxt",
    register_as = "FaceLandmarksDetectorCpu",
    deps = [
        "//mediapipe/calculators/core:gate_calculator",
        "//mediapipe/calculators/core:split_vector_calculator",
        "//mediapipe/calculators/tensor:image_to_tensor_calculator",
        "//mediapipe/calculators/tensor:inference_calculator",
        "//mediapipe/calculators/tensor:tensors_to_floats_calculator",
        "//mediapipe/calculators/tensor:tensors_to_landmarks_calculator",
        "//mediapipe/calculators/util:landmark_projection_calculator",
        "//mediapipe/calculators/util:thresholding_calculator",
    ],
)

mediapipe_simple_subgraph(
    name = "face_landmarks_detector_gpu",
    graph = "face_landmarks_detector_gpu.pbtxt",
    register_as = "FaceLandmarksDetectorGpu",
    deps = [
        "//mediapipe/calculators/core:gate_calculator",
        "//mediapipe/calculators/core:split_vector_calculator",
        "//mediapipe/calculators/tensor:image_to_tensor_calculator",
        "//mediapipe/calculators/tensor:inference_calculator",
        "//mediapipe/calculators/tensor:tensors_to_floats_calculator",
        "//mediapipe/calculators/tensor:tensors_to_landmarks_calculator",
        "//mediapipe/calculators/util:landmark_projection_calculator",
        "//mediapipe/calculators/util:thresholding_calculator",
    ],
)

mediapipe_simple_subgraph(
    name = "face_landmarks_detector_front_cpu",
    graph = "face_landmarks_detector_front_cpu.pbtxt",
    register_as = "FaceLandmarksDetectorFrontCpu",
    deps = [
        ":face_landmarks_detector_cpu",
        "//mediapipe/modules/face_landmark:face_detection_front_detection_to_roi",
        "//mediapipe/modules/face_landmark:face_landmark_landmarks_to_roi",
        "//mediapipe/calculators/core:begin_loop_calculator",
        "//mediapipe/calculators/core:clip_vector_size_calculator",
        "//mediapipe/calculators/core:constant_side_packet_calculator",
        "//mediapipe/calculators/core:end_loop_calculator",
        "//mediapipe/calculators/core:gate_calculator",
        "//mediapipe/calculators/core:previous_loopback_calculator",
        "//mediapipe/calculators/image:image_properties_calculator",
        "//mediapipe/calculators/util:association_norm_rect_calculator",
        "//mediapipe/calculators/util:collection_has_min_size_calculator",
        "//mediapipe/modules/face_detection:face_detection_short_range_cpu",
        ":counting_vector_size_calculator",
    ],
)

mediapipe_simple_subgraph(
    name = "face_landmarks_detector_front_gpu",
    graph = "face_landmarks_detector_front_gpu.pbtxt",
    register_as = "FaceLandmarksDetectorFrontGpu",
    deps = [
        ":face_landmarks_detector_gpu",
        "//mediapipe/modules/face_landmark:face_detection_front_detection_to_roi",
        "//mediapipe/modules/face_landmark:face_landmark_landmarks_to_roi",
        "//mediapipe/calculators/core:begin_loop_calculator",
        "//mediapipe/calculators/core:clip_vector_size_calculator",
        "//mediapipe/calculators/core:constant_side_packet_calculator",
        "//mediapipe/calculators/core:end_loop_calculator",
        "//mediapipe/calculators/core:gate_calculator",
        "//mediapipe/calculators/core:previous_loopback_calculator",
        "//mediapipe/calculators/image:image_properties_calculator",
        "//mediapipe/calculators/util:association_norm_rect_calculator",
        "//mediapipe/calculators/util:collection_has_min_size_calculator",
        "//mediapipe/modules/face_detection:face_detection_short_range_gpu",
        ":counting_vector_size_calculator",
    ],
)

cc_library(
    name = "desktop_live_calculators",
    deps = [
        "//mediapipe/calculators/core:constant_side_packet_calculator",
        "//mediapipe/calculators/core:flow_limiter_calculator",
        "//mediapipe/graphs/face_mesh/subgraphs:face_renderer_cpu",
        ":face_landmarks_detector_front_cpu",
    ],
)

cc_library(
    name = "mobile_calculators",
    deps = [
        "//mediapipe/calculators/core:flow_limiter_calculator",
        "//mediapipe/graphs/face_mesh/subgraphs:face_renderer_gpu",
        ":face_landmarks_detector_front_gpu",
    ],
)

cc_binary(
    name = "face_landmarks_detector",
    srcs = [
        "face_landmarks_detector.h",
        "export.h",
        "face_landmarks_detector.cc"
    ],
    deps = [
        "//mediapipe/framework:calculator_framework",
        "//mediapipe/framework/formats:image_frame",
        "//mediapipe/framework/formats:image_frame_opencv",
        "//mediapipe/framework/port:file_helpers",
        "//mediapipe/framework/port:opencv_highgui",
        "//mediapipe/framework/port:opencv_imgproc",
        "//mediapipe/framework/port:opencv_video",
        "//mediapipe/framework/port:parse_text_proto",
        "//mediapipe/framework/formats:landmark_cc_proto",
        "//mediapipe/framework/port:status",
        "@com_google_absl//absl/flags:flag",
        "@com_google_absl//absl/flags:parse",
        ":desktop_live_calculators",
    ],
    defines = ["COMPILING_FACE_DETECTOR_API"],
    linkshared = 1,
)

cc_library(
    name = "face_landmarks_detector_ios_lib",
    srcs = [
        "face_landmarks_detector.cc",
    ],
    hdrs = [
        "export.h",
        "face_landmarks_detector.h"
    ],
    copts = ["-std=c++17"],
    deps = [
        "//mediapipe/framework:calculator_framework",
        "//mediapipe/framework/port:parse_text_proto",
        "//mediapipe/graphs/face_mesh/subgraphs:face_renderer_gpu",
        "//mediapipe/calculators/core:flow_limiter_calculator",
        ":face_landmarks_detector_front_gpu",
        "//mediapipe/calculators/core:constant_side_packet_calculator",
        ":desktop_live_calculators",
    ] + select({
        "//mediapipe:ios_i386": [],
        "//mediapipe:ios_x86_64": [],
        "//conditions:default": [
            ":mobile_calculators",
            "//mediapipe/framework/formats:landmark_cc_proto",
        ],
    }),
    defines = ["COMPILING_FACE_DETECTOR_API"],
    alwayslink = 1,
)

apple_binary(
    name = "face_landmarks_detector_ios",
    deps = [
        ":face_landmarks_detector_ios_lib",
        "@ios_opencv//:OpencvFramework"
    ],
    data = [
        "//mediapipe/modules/face_detection:face_detection_short_range.tflite",
        "//mediapipe/modules/face_landmark:face_landmark.tflite",
    ],
    platform_type = "ios",
    binary_type = "dylib"
)
