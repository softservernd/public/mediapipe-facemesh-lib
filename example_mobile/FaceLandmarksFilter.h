#ifndef MEDIAPIPE_FACE_LANDMARKS_FILTER
#define MEDIAPIPE_FACE_LANDMARKS_FILTER

#include <QAbstractVideoFilter>
#include <QVideoFilterRunnable>


namespace Mediapipe {
struct FaceDetector;
}

namespace cv {
struct Mat;
}

struct QOpenGLFunctions;

class FaceLandmarksFilter : public QAbstractVideoFilter
{
    Q_OBJECT
public:
    FaceLandmarksFilter( QObject* parent = nullptr );
    QVideoFilterRunnable* createFilterRunnable() Q_DECL_OVERRIDE;
};

class FaceLandmarksFilterRunnable : public QVideoFilterRunnable
{
public:
    FaceLandmarksFilterRunnable();
    QVideoFrame run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags) Q_DECL_OVERRIDE;

private:
    QImage videoFrameToImage(const QVideoFrame &videoFrame);
    cv::Mat imageToCvMat(const QImage &input);
    
    std::unique_ptr<Mediapipe::FaceDetector> m_faceLandmarksDetector;
    QOpenGLFunctions* m_glCtxFuncs;
};

#endif // MEDIAPIPE_FACE_LANDMARKS_FILTER
